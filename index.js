
var report = [
	{	english: "I am called David and I study Chinese",
		chinese: [
			{	english:	"I",
				pinyin:		"wǒ",
				chinese:	"我",
			},
			{	english:	"called",
				pinyin:		"jiào",
				chinese:	"叫",
			},
			{	english:	"David",
				pinyin:		"Dàwèi",
				chinese:	"大卫",
			},
			{	english:	"also",
				pinyin:		"yě",
				chinese:	"也",
			},
			{	english:	"study",
				pinyin:		"xué",
				chinese:	"学",
			},
			{	english:	"Chinese",
				pinyin:		"Zhōngwén",
				chinese:	"中文",
			},
		],
	},
	{	english: "My classmates and I ate dinner at a Chinese restaurant",
		chinese: [
			{	english:	"I",
				pinyin:		"wǒ",
				chinese:	"我",
			},
			{	english:	"and",
				pinyin:		"hé",
				chinese:	"和",
			},
			{	english:	"my",
				pinyin:		"wǒde",
				chinese:	"我的",
			},
			{	english:	"classmates",
				pinyin:		"tōngxuémen",
				chinese:	"同学们",
			},
			{	english:	"ate",
				pinyin:		"chī",
				chinese:	"吃",
			},
			{	english:	"dinner",
				pinyin:		"wǎnfàn",
				chinese:	"晚饭",
			},
			{	english:	"at",
				pinyin:		"zài",
				chinese:	"在",
			},
			{	english:	"a",
				pinyin:		"yījiā",
				chinese:	"一家",
			},
			{	english:	"Chinese",
				pinyin:		"Zhōngguó",
				chinese:	"中国",
			},
			{	english:	"restaurant",
				pinyin:		"fànguǎn",
				chinese:	"饭馆",
			},
		],
	},
	{	english: "We ordered our meals in Chinese",
		chinese: [
			{	english:	"we",
				pinyin:		"wǒmen",
				chinese:	"我们",
			},
			{	english:	"ordered food",
				pinyin:		"diǎncài",
				chinese:	"点菜",
			},
			{	english:	"in",
				pinyin:		"zài",
				chinese:	"在",
			},
			{	english:	"Chinese",
				pinyin:		"Zhōngwén",
				chinese:	"中文。",
			},
		],
	},
	{	english: "First we all had tea and crab rangoon with sweet and sour sauce",
		chinese: [
			{	english:	"we",
				pinyin:		"wǒmen",
				chinese:	"我们",
			},
			{	english:	"first",
				pinyin:		"xiān",
				chinese:	"先",
			},
			{	english:	"all",
				pinyin:		"dōu",
				chinese:	"都",
			},
			{	english:	"had",
				pinyin:		"yǒu",
				chinese:	"有",
			},
			{	english:	"tea",
				pinyin:		"chá",
				chinese:	"茶",
			},
			{	english:	"also",
				pinyin:		"yě",
				chinese:	"也",
			},
			{	english:	"crab dumpling",
				pinyin:		"xiè jiǎo",
				chinese:	"蟹饺",
			},
			{	english:	"with",
				pinyin:		"gēn",
				chinese:	"跟",
			},
			{	english:	"sweet & sour sauce",
				pinyin:		"tían suán jiàng",
				chinese:	"甜酸酱",
			},
		],
	},
	{	english: "I had hot & sour soup and General Zou's chicken",
		chinese: [
			{	english:	"I",
				pinyin:		"Wǒ",
				chinese:	"我",
			},
			{	english:	"had",
				pinyin:		"yǒu",
				chinese:	"有",
			},
			{	english:	"sour & spicy soup",
				pinyin:		"suān là tāng",
				chinese:	"酸辣汤",
			},
			{	english:	"and",
				pinyin:		"hé",
				chinese:	"和",
			},
			{	english:	"General Zou's chicken",
				pinyin:		"Zūo Gōng jī",
				chinese:	"左公鸡",
			},
		],
	},
	{	english: "Teacher, for all of us,  ordered steamed rice and fried rice with pork for all of us",
		chinese: [
			{	english:	"teacher",
				pinyin:		"Lǎoshī",
				chinese:	"老师",
			},
			{	english:	"for",
				pinyin:		"gěi",
				chinese:	"给",
			},
			{	english:	"us",
				pinyin:		"wǒmen",
				chinese:	"我们",
			},
			{	english:	"all",
				pinyin:		"dōu",
				chinese:	"都",
			},
			{	english:	"ordered",
				pinyin:		"diǎn",
				chinese:	"点",
			},
			{	english:	"steamed rice",
				pinyin:		"báifàn",
				chinese:	"白饭",
			},
			{	english:	"and",
				pinyin:		"hé",
				chinese:	"和",
			},
			{	english:	"fried rice",
				pinyin:		"chǎofàn",
				chinese:	"炒饭",
			},
			{	english:	"with",
				pinyin:		"gēn",
				chinese:	"跟",
			},
			{	english:	"pork",
				pinyin:		"zhūròu",
				chinese:	"猪肉",
			},
		],
	},
	{	english: "Some of us ate with a pair of chopsticks",
		chinese: [
			{	english:	"We",
				pinyin:		"Wǒmen",
				chinese:	"我",
			},
			{	english:	"not",
				pinyin:		"bù",
				chinese:	"不",
			},
			{	english:	"all",
				pinyin:		"dōu",
				chinese:	"都",
			},
			{	english:	"ate",
				pinyin:		"chī",
				chinese:	"吃",
			},
			{	english:	"with",
				pinyin:		"gēn",
				chinese:	"跟",
			},
			{	english:	"a pair of chopsticks",
				pinyin:		"yī shuāng kuàizi",
				chinese:	"一双筷子",
			},
			{	english:	"",
				pinyin:		"",
				chinese:	"",
			},
		],
	},
	{	english: "We mostly spoke English",
		chinese: [
			{	english:	"we",
				pinyin:		"Wǒmen",
				chinese:	"我们",
			},
			{	english:	"mostly",
				pinyin:		"dà duō",
				chinese:	"大多",
			},
			{	english:	"spoke",
				pinyin:		"shuō",
				chinese:	"说",
			},
			{	english:	"English",
				pinyin:		"Yīngwén",
				chinese:	"英文",
			},
		],
	},
	{	english: "The food was ok but the conversation was very good",
		chinese: [
			{	english:	"food",
				pinyin:		"Cài",
				chinese:	"菜",
			},
			{	english:	"was",
				pinyin:		"shì",
				chinese:	"是",
			},
			{	english:	"ok",
				pinyin:		"hái hǎo",
				chinese:	"还好",
			},
			{	english:	"but",
				pinyin:		"kěshì",
				chinese:	"可是",
			},
			{	english:	"conversation",
				pinyin:		"tán huà",
				chinese:	"谈话",
			},
			{	english:	"was",
				pinyin:		"shì",
				chinese:	"是",
			},
			{	english:	"very",
				pinyin:		"hěn hǎo",
				chinese:	"很",
			},
			{	english:	"good",
				pinyin:		"hǎo",
				chinese:	"好",
			},
		],
	},
//	{	english: "",
//		chinese: [
//			{	english:	"",
//				pinyin:		"",
//				chinese:	"",
//			},
//		],
//	},
];

build = function() {
	console.log("hello world");
	content = document.getElementById("content");
	for(i=0; sentence = report[i]; i++) {
		ns = document.createElement('span');
		ns.className = "sentence";
		if(sentence.english) ns.setAttribute("english",sentence.english);
		for(j=0; word = sentence.chinese[j]; j++) {
			nw = document.createElement('span');
			nw.className = "word";
			if(word.english) nw.setAttribute("english",word.english);
			if(word.pinyin) nw.setAttribute("pinyin",word.pinyin);
			for(k=0; character = word.chinese.charAt(k); k++) {
				nc = document.createElement("span");
				nc.className = 'character';
				nc.appendChild(document.createTextNode(character));
				nw.appendChild(nc);
			}
			ns.appendChild(nw);
		}
		content.appendChild(ns);
	}
}

